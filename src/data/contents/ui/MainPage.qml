/*
 *   Copyright 2015 Marco Martin <mart@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Library General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.1
import QtQuick.Controls 2.0 as QQC2
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.4 as Kirigami
import "." as App

Kirigami.ScrollablePage {
    id: pageRoot

    implicitWidth: Kirigami.Units.gridUnit * 20
    background: Rectangle {
        color: Kirigami.Theme.backgroundColor
    }

    title: qsTr("Summary")

    //flickable: mainListView
    actions {
        contextualActions: [
            Kirigami.Action {
                text:"Action 1"
                iconName: "document-decrypt"
                onTriggered: showPassiveNotification("Action 1 clicked")
            },
            Kirigami.Action {
                id: shareAction
                visible: checkableAction.checked
                text:"Action 2"
                iconName: "document-share"
                onTriggered: showPassiveNotification("Action 2 clicked")
            },
            Kirigami.Action {
                id: checkableAction
                text:"Checkable"
                checkable: true
                iconName: "dashboard-show"
                onCheckedChanged: showPassiveNotification("Checked: " + checked)
            }
        ]
    }
    Kirigami.CardsLayout {
        minimumColumnWidth: Kirigami.Units.gridUnit * 20
        App.Event {
            model: ListModel {
                id: eventsModel
                ListElement {
                    eventName: "Event 1"
                    date: "Today"
                    time: "10:00 - 16:00"
                }
                ListElement {
                    eventName: "Event 3"
                    date: "Today"
                    time: "10:05 - 16:00"
                }
                ListElement {
                    eventName: "Event 2"
                    date: "Tomorrow"
                    time: "12:00 - 16:00"
                }
            }
        }
        App.ToDo {
            model: ListModel {
                id: toDosModel
                ListElement {
                    toDoName: "ToDo 1"
                    dueDate: "Today"
                    completion: "56"
                }
                ListElement {
                    toDoName: "ToDo 2"
                    dueDate: "Today"
                    completion: "57"
                }
            }
        }
    }
}

